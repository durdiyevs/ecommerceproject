package com.DS.amazonutility;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;



import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonUtility {

	
	// Class level variable, global variable, field

	private static WebDriver driver=null;
	
	
	public  Actions act ;

	public AmazonUtility() {

		/*
		 * First function that works when you create an object is contructor.
		 * Constructors are used to assign values to the class variables at the time of
		 * object creation. Static class level variables only can be accessed by static
		 * methods. Static methods do not create objects in memory: Faster,
		 * optimization, no memory usage
		 */

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.com");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}	public static void startApplication() {
		
		if( driver == null) {
			
			AmazonUtility amazonUtulity = new AmazonUtility();
			
		} 
		
	}public static void closeApplication() {
			
			if(driver!=null) {
				
				driver.close();
				driver.quit();
			}

	}

	public static WebDriver getDriver() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
